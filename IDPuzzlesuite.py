import sys
from PySide2 import QtCore, QtWidgets, QtGui
from pyidp3.typedIDP import IDP


class Control_Widget(QtWidgets.QWidget):
    """
    This widget is a collection of information and controls needed for the
    game.
    """
    def __init__(self, main):
        """
        Initializes the widget.

        :param main: the main controller widget
        :returns None:
        """
        super().__init__()

        self.main = main
        self.nb_moves = 0
        self._tile_names = {0: None, 1: "Images/wall.png",
                            2: "Images/crate.png",
                            3: "Images/crate2.png",
                            4: "Images/location.png",
                            5: "Images/keeper.png"}

        self.init_ui()

    def init_ui(self):
        """
        Initializes every UI element, and adds it to the layout.

        :returns None:
        """
        self.label_moves = QtWidgets.QLabel(str("Move count"))
        self.label_moves.setAlignment(QtCore.Qt.AlignCenter |
                                      QtCore.Qt.AlignCenter)
        self.label_nb_moves = QtWidgets.QLabel(str(self.nb_moves))
        self.label_nb_moves.setAlignment(QtCore.Qt.AlignCenter |
                                         QtCore.Qt.AlignCenter)
        self.label_solvable = QtWidgets.QLabel(str(""))
        self.label_solvable.setAlignment(QtCore.Qt.AlignCenter |
                                         QtCore.Qt.AlignCenter)

        self.btn_find_sol = QtWidgets.QPushButton('Find solution')
        self.btn_find_sol.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_find_sol.clicked.connect(self.main.find_sol)
        self.btn_find_opt_sol = QtWidgets.QPushButton('Find optimal solution')
        self.btn_find_opt_sol.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_find_opt_sol.clicked.connect(self.main.find_opt_sol)
        self.btn_check_solv = QtWidgets.QPushButton('Check solvability')
        self.btn_check_solv.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_check_solv.clicked.connect(self.main.check_solv)

        self.label_width = QtWidgets.QLabel("Field Width:")
        self.label_width.setAlignment(QtCore.Qt.AlignCenter |
                                      QtCore.Qt.AlignCenter)
        self.label_height = QtWidgets.QLabel("Field height:")
        self.label_height.setAlignment(QtCore.Qt.AlignCenter |
                                       QtCore.Qt.AlignCenter)
        self.input_width = QtWidgets.QLineEdit('6')
        self.input_height = QtWidgets.QLineEdit('8')
        self.btn_gen_puz = QtWidgets.QPushButton('Generate puzzle')
        self.btn_gen_puz.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_gen_puz.clicked.connect(self.main.gen_puzzle)

        ico_left = QtGui.QIcon('./Images/arrow_left.png')
        self.btn_left = QtWidgets.QPushButton(ico_left, "")
        self.btn_left.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_left.clicked.connect(self.main.undo_move)
        ico_right = QtGui.QIcon('./Images/arrow_right.png')
        self.btn_right = QtWidgets.QPushButton(ico_right, "")
        self.btn_right.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_right.clicked.connect(self.main.redo_move)

        self.icon_buttons = []
        self.icon_group = QtWidgets.QButtonGroup()
        for i, tile in enumerate(self._tile_names.values()):
            ico = QtGui.QIcon(tile)
            btn = QtWidgets.QPushButton(ico, "")
            btn.setFocusPolicy(QtCore.Qt.NoFocus)
            btn.setCheckable(True)
            self.icon_buttons.append(btn)
            self.icon_group.addButton(btn)
            self.icon_group.setId(btn, i)

        self.btn_save_puz = QtWidgets.QPushButton('Save Puzzle')
        self.btn_save_puz.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_save_puz.clicked.connect(self.main.save_puz)
        self.btn_load_puz = QtWidgets.QPushButton('Load Puzzle')
        self.btn_load_puz.setFocusPolicy(QtCore.Qt.NoFocus)
        self.btn_load_puz.clicked.connect(self.main.load_puz)

        self.tile_size = None

        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.label_moves, 0, 1,)
        self.layout.addWidget(self.btn_left, 1, 0)
        self.layout.addWidget(self.label_nb_moves, 1, 1)
        self.layout.addWidget(self.btn_right, 1, 2)
        self.layout.addWidget(self.label_solvable, 0, 0)
        self.layout.addWidget(self.btn_find_sol, 2, 1)
        self.layout.addWidget(self.btn_find_opt_sol, 2, 2)
        self.layout.addWidget(self.btn_check_solv, 2, 0)
        self.layout.addWidget(self.label_width, 0, 4)
        self.layout.addWidget(self.label_height, 1, 4)
        self.layout.addWidget(self.input_width, 0, 5)
        self.layout.addWidget(self.input_height, 1, 5)
        self.layout.addWidget(self.btn_gen_puz, 2, 5)
        btn_x = 6
        btn_y = 0
        for btn in self.icon_buttons:
            self.layout.addWidget(btn, btn_y, btn_x)
            btn_y += 1
            if btn_y > 2:
                btn_x += 1
                btn_y = 0

        self.layout.addWidget(self.btn_load_puz, 0, 8)
        self.layout.addWidget(self.btn_save_puz, 1, 8)
        self.setLayout(self.layout)

    def do_move(self, direction):
        """
        Method to perform a move.
        All this does is increment the number of moves taken.

        :returns None:
        """
        self.nb_moves += 1
        self.label_nb_moves.setText(str(self.nb_moves))
        if self.label_solvable.text() != "":
            self.label_solvable.setText("")
        self.repaint()

    def undo_move(self):
        """
        Method to perform a move.
        All this does is decrement the number of moves taken.

        :returns None:
        """
        self.nb_moves -= 1
        self.label_nb_moves.setText(str(self.nb_moves))
        if self.label_solvable.text() != "":
            self.label_solvable.setText("")
        self.repaint()

    def reset(self):
        """
        Method to reset the number of moves.

        :returns None:
        """
        self.nb_moves = 0
        self.label_nb_moves.setText(str(self.nb_moves))
        self.repaint()

    def set_solvable(self, solvable):
        """
        Method to display the solvability of the puzzle.

        :param Boolean: True if solvable, False if not.
        :returns None:
        """
        if solvable:
            self.label_solvable.setText("Solvable!")
        else:
            self.label_solvable.setText("Unsolvable!")

    def get_dimensions(self):
        """
        Method to fetch the values of the dimension inputs.

        :returns Tuple(int, int): the width and height in the input fields.
        """
        return (int(self.input_width.text()), int(self.input_height.text()))

    def set_dimensions(self, width, height):
        """
        Set the width and the height in the input fields.

        :returns None:
        """
        self.input_width.setText(width)
        self.input_height.setText(height)


class Play_Grid_Widget(QtWidgets.QWidget):
    """
    This widget draws the play grid.
    The grid consists of a number of tiles, which are given in the form of a
    two-dimensional list of integers. Every integer in this array represents a
    different tile, as listed in the `_tile_names` dictionary.
    """
    def __init__(self, field, main, game):
        """
        Initialize the widget.

        :param [[int,],]: a two dimensional array representing the playing
        field.
        :param QWidget: main, the main control widget.
        """
        super().__init__()

        # Set the widget as focus when it is clicked or when it is tabbed to.
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        self.main = main

        self._field = field
        if game == "sokoban":
            self._tile_names = {0: None,
                                1: "Images/wall.png",
                                2: "Images/crate.png",
                                3: "Images/crate2.png",
                                4: "Images/location.png",
                                5: "Images/keeper.png"}
        elif game == "sudoku":
            self._tile_names = {0: None,
                                1: "Images/one.png",
                                2: "Images/two.png",
                                3: "Images/three.png",
                                4: "Images/four.png",
                                5: "Images/five.png",
                                6: "Images/six.png",
                                7: "Images/seven.png",
                                8: "Images/eight.png",
                                9: "Images/nine.png"}

        self.tile_size = None
        self.margin_x = None
        self.margin_y = None

    def paintEvent(self, e):
        """
        Magic method to redraw the field.
        This method is called using `repaint()` or when the window resizes.

        :returns None:
        """
        qp = QtGui.QPainter()
        qp.begin(self)
        self.draw_grid(qp)
        qp.end()

    def draw_grid(self, qp):
        """
        Method which draws the grid.
        Calculates the size of the grid and the tiles, and then draws them all.

        :param QtGui.QPainter(): the painter instance with which to draw.
        :returns None:
        """
        # Find the size of the drawing area + other var.
        size = self.size()
        height = size.height()
        width = size.width()
        if height < width:
            min_dim = height
            min_dim_field = len(self._field)
        else:
            min_dim = width
            min_dim_field = len(self._field[0])
        tile_size = min_dim/min_dim_field
        margin_x = abs((width - tile_size * len(self._field[0]))/2)
        margin_y = abs((height - tile_size * len(self._field))/2)

        # Set the QPainter
        qp.setPen(QtCore.Qt.red)
        qp.setBrush(QtGui.QColor(200, 0, 0))

        # Paint the tiles.
        coord = [margin_x, margin_y]
        for row in self._field:
            qp.drawLine(margin_x, coord[1], width-margin_x, coord[1])
            for tile in row:
                qp.drawLine(coord[0], margin_y, coord[0], height-margin_y)
                # Draw tile.
                self.__draw_tile(qp, coord, tile_size, tile_size,
                                 self._tile_names[tile])
                # Update x-coordinates.
                coord[0] += tile_size
            # Update x-, y-coordinates
            coord[0] = margin_x
            coord[1] += tile_size

            qp.drawLine(coord[0], margin_y, coord[0], height-margin_y)
        qp.drawLine(margin_x, coord[1], width-margin_x, coord[1])

        # Set these variables.
        self.tile_size = tile_size
        self.margin_x = margin_x
        self.margin_y = margin_y

    def __draw_tile(self, qp, coord, width, height, filename):
        """
        Method to draw a tile of width `width`, height `height` at coordinates
        `coord` using the image `filename`.

        :param QtGui.QPainter(): the QPainter instance with which to draw.
        :param (int, int): coord, a tuple containing the x and y position of
            the top-left corner of the position of the tile.
        :param int: width, the width of the tile.
        :param int: height, the height of the tile.
        :param str: filename, the filename.
        :returns None:
        """
        if filename is None:
            return
        img = QtGui.QImage(filename)
        rect = QtCore.QRectF(coord[0], coord[1],
                             width, height)
        qp.drawImage(rect, img)

    def update_grid(self, grid):
        """
        Method to redraw the grid.

        :param [[int,],]: grid, a 2D list containing the ID's of the tiles.
        :returns None:
        """
        self._field = grid
        self.repaint()

    def mousePressEvent(self, event):
        """
        Event handler method which is fired when a mouse press is recorded in
        the playing filed.
        It checks whether a tile was clicked, and calls the controller's
        `toggle_tile` method.

        :param event:
        :returns None:
        """
        # Check whether the click was in the field.
        if (self.margin_x < event.x() < self.size().width() - self.margin_x and
           self.margin_y < event.y() < self.size().height() - self.margin_y):
            # Calculate the tile's coordinates.
            tile_x = int((event.x() - self.margin_x) // self.tile_size)
            tile_y = int((event.y() - self.margin_y) // self.tile_size)
            self.main.toggle_tile(tile_x, tile_y)


class Main_Widget(QtWidgets.QWidget):
    """
    The main controller widget.
    This widget creates three classes:

        * play_grid, the Widget which draws the play grid.
        * controls, the Widget which contains all control related concepts.
        * game, an object which contains the logic of the game.
    """
    def __init__(self, game):
        """
        Initialize the main widget.
        """
        super().__init__()

        # Init the game.
        if game == "sokoban":
            self.game = Sokoban()
        elif game == "sudoku":
            self.game = Sudoku()
        field = self.game.generate_field()

        # Create the widgets.
        self.play_grid = Play_Grid_Widget(field, self, game)
        self.controls = Control_Widget(self)

        # Create the layout and add the widgets.
        self.layout = QtWidgets.QGridLayout()
        self.layout.setSpacing(10)
        # self.layout.addWidget(self.controls)
        self.layout.addWidget(self.controls, 0, 0)
        self.layout.addWidget(self.play_grid, 1, 0, 5, 0)
        self.setLayout(self.layout)

    def keyPressEvent(self, e):
        """
        Key press event handler function.

        :param event:
        :returns None:
        """
        gen = False
        if e.key() == QtCore.Qt.Key_Left:
            self.move("left")
            gen = True
        elif e.key() == QtCore.Qt.Key_Right:
            self.move("right")
            gen = True
        elif e.key() == QtCore.Qt.Key_Up:
            self.move("up")
            gen = True
        elif e.key() == QtCore.Qt.Key_Down:
            self.move("down")
            gen = True
        elif e.key() == QtCore.Qt.Key_R:
            self.game.reset()
            self.controls.reset()
            gen = True

        if gen is True:
            field = self.game.generate_field()
            self.play_grid.update_grid(field)

    def move(self, direction):
        """
        Method to process a move in any direction.
        The control and game widgets need to be adjusted.

        :param str: direction, either 'left', 'up', 'down' or 'right'.
        :returns None:
        """
        if self.game.do_move(direction):
            self.controls.do_move(direction)

    def undo_move(self, direction):
        """
        Method to go back to a previous move.

        :param str: direction, either 'left', 'up', 'down' or 'right'.
        :returns None:
        """
        if self.game.undo_move():
            field = self.game.generate_field()
            self.play_grid.update_grid(field)
            self.controls.undo_move()

    def redo_move(self):
        """
        Method to redo a previously undone move, or go to the next move.

        :returns None:
        """
        if self.game.redo_move():
            field = self.game.generate_field()
            self.play_grid.update_grid(field)
            self.controls.do_move(None)

    def find_sol(self):
        """
        Method to find a solution for the puzzle.

        :returns None:
        """
        self.game.find_sol()
        field = self.game.generate_field()
        self.play_grid.update_grid(field)

    def find_opt_sol(self):
        """
        Method to find an optimal solution for the puzzle.

        :returns None:
        """
        self.game.find_opt_sol()
        field = self.game.generate_field()
        self.play_grid.update_grid(field)

    def check_solv(self):
        """
        Method to check the solvability of a puzzle.

        :returns None:
        """
        solvable = self.game.check_solv()
        self.controls.set_solvable(solvable)

    def gen_puzzle(self):
        """
        Method to generate a new puzzle.

        :returns None:
        """
        dimensions = self.controls.get_dimensions()
        if self.game.gen_puzzle(dimensions):
            field = self.game.generate_field()
            self.play_grid.update_grid(field)

    def toggle_tile(self, x, y):
        """
        Method to toggle a tile.

        :param int: x, the x of the tile to toggle.
        :param int: y, the y of the tile to toggle.
        :returns None:
        """
        tile = self.controls.icon_group.checkedId()
        if self.game.toggle_tile(x, y, tile):
            field = self.game.generate_field()
            self.play_grid.update_grid(field)

    def load_puz(self):
        """
        Method to load a puzzle from file.

        :returns None:
        """
        options = QtWidgets.QFileDialog.Options()
        # options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self,
                                                            options=options)
        if fileName:
            with open(fileName, "r") as fp:
                dimensions = fp.readline().strip()
                input_el = fp.readline()
        width, height = dimensions.split(",")
        width = int(width)
        height = int(height)

        input_el = input_el.split(",")
        field = []
        line = []
        for el in input_el:
            line.append(int(el))
            if len(line) == width:
                field.append(line)
                line = []
        self.game.set_field(field)
        self.play_grid.update_grid(field)
        self.controls.set_dimensions(str(width), str(height))

    def save_puz(self):
        """
        Method to save a puzzle to file.

        :returns None:
        """
        field = self.game.generate_field()
        output = []
        for line in field:
            for element in line:
                output.append(str(element))
        options = QtWidgets.QFileDialog.Options()
        # options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,
                                                            options=options)
        if fileName:
            with open(fileName, "w") as fp:
                fp.write("{}, {}\n".format(len(field[0]), len(field)))
                fp.write(",".join(output))
                fp.write("\n")


class Game():
    def __init__(self):
        raise ValueError("Method not implemented")

    def generate_field(self):
        raise ValueError("Method not implemented")

    def set_field(self, field):
        raise ValueError("Method not implemented")

    def do_move(self, direction):
        raise ValueError("Method not implemented")

    def undo_move(self):
        raise ValueError("Method not implemented")

    def redo_move(self):
        raise ValueError("Method not implemented")

    def check_solv(self):
        raise ValueError("Method not implemented")

    def find_sol(self):
        raise ValueError("Method not implemented")

    def find_opt_sol(self):
        raise ValueError("Method not implemented")

    def reset(self):
        raise ValueError("Method not implemented")

    def run_game(self, params, opt=False, nb_box=False, box_end=False,
                 nb_models=1):
        raise ValueError("Method not implemented")

    def toggle_tile(self, x, y, target_tile):
        raise ValueError("Method not implemented")

    def remove_tile(self, x, y):
        raise ValueError("Method not implemented")

    def add_tile(self, x, y, tile_type):
        raise ValueError("Method not implemented")


class Sokoban(Game):
    def __init__(self):
        # self.idp = IDP('idp')
        # self.run_game()

        self.movecounter = 0
        self._boxes_pos = {}
        self._keeper_pos = {}

        self.params = {'col': (0, 5),
                       'row': (0, 7),
                       'time': (0, 60),
                       'nb_moves': None,
                       'box_start': [(3, 1), (2, 2), (3, 3), (2, 4), (2, 5)],
                       'box_end': [(1, 1), (2, 1), (3, 1), (4, 1), (1, 2)],
                       'box': None,
                       'keeper_start': [(1, 5)],
                       'keeper': None,
                       'left': None,
                       'right': None,
                       'up': None,
                       'down': None,
                       'wall': [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0),
                                (0, 1), (5, 1), (0, 2), (5, 2), (0, 3), (1, 3),
                                (5, 3), (0, 4), (1, 4), (4, 4), (5, 4), (0, 5),
                                (4, 5), (0, 6), (1, 6), (4, 6), (1, 7), (2, 7),
                                (3, 7), (4, 7)]}
        self._boxes_pos[0] = self.params['box_start']
        self._keeper_pos[0] = self.params['keeper_start']

        self.generate_field()

    def generate_field(self):
        """
        Functions which generates the game field.
        Every block type has it's own identifier:

        * 0 = void
        * 1 = wall
        * 2 = crate
        * 3 = crate on location
        * 4 = location
        * 5 = keeper

        """
        field = []
        for r in range(self.params['row'][0], self.params['row'][1]+1):
            row = []
            for c in range(self.params['col'][0], self.params['col'][1]+1):
                if (c, r) in self.params['wall']:
                    row.append(1)
                elif (c, r) in self.params['keeper_start']:
                    row.append(5)
                elif self.params['box_end'] and (c, r) in self.params['box_end']:
                    if self.params['box_start'] and (c, r) in self.params['box_start']:
                        row.append(3)
                    elif self.params['box'] and (c, r) in self.params['box']:
                        row.append(3)
                    else:
                        row.append(4)
                elif self.params['box'] and (c, r) in self.params['box']:
                    row.append(2)
                elif self.params['box_start'] and (c, r) in self.params['box_start']:
                    row.append(2)
                else:
                    row.append(0)
            field.append(row)
        return field

    def set_field(self, field):
        """
        Method to generate the predicates based on the field.

        * 0 = void
        * 1 = wall
        * 2 = crate
        * 3 = crate on location
        * 4 = location
        * 5 = keeper
        """
        # Clear out the old variables.
        self.params = {'col': (0, len(field[0])),
                       'row': (0, len(field)),
                       'time': (0, 100),
                       'nb_moves': None,
                       'box_start': [],
                       'box_end': [],
                       'box': None,
                       'keeper_start': [],
                       'keeper': None,
                       'left': None,
                       'right': None,
                       'up': None,
                       'down': None,
                       'wall': []}
        # Set the new variables.
        for r, line in enumerate(field):
            for c, el in enumerate(line):
                if el == 0:
                    continue
                elif el == 1:
                    self.add_tile(c, r, 'wall')
                elif el == 2:
                    self.add_tile(c, r, 'box_start')
                elif el == 3:
                    self.add_tile(c, r, 'box_end')
                    self.add_tile(c, r, 'box_start')
                elif el == 4:
                    self.add_tile(c, r, 'box_end')
                elif el == 5:
                    self.add_tile(c, r, 'keeper_start')

    def do_move(self, direction):
        """
        This function checks if the character is able to move in a direction
        supplied by the user.
        If the move is possible, it is made.

        It works by changing the params dict to a version which allows us to
        check whether a model can be found in which the movement is allowed.
        """
        # Copy the current set of params.
        params = self.params.copy()

        # Set the keeper and keeper_start values depending on the direction.
        keeper_loc = params['keeper_start'][0]
        if direction == "right":
            params['keeper'] = [(0, keeper_loc[0], keeper_loc[1]),
                                (1, keeper_loc[0]+1, keeper_loc[1])]
            params['keeper_start'] = None
        elif direction == "left":
            params['keeper'] = [(0, keeper_loc[0], keeper_loc[1]),
                                (1, keeper_loc[0]-1, keeper_loc[1])]
            params['keeper_start'] = None
        elif direction == "up":
            params['keeper'] = [(0, keeper_loc[0], keeper_loc[1]),
                                (1, keeper_loc[0], keeper_loc[1]-1)]
            params['keeper_start'] = None
        elif direction == "down":
            params['keeper'] = [(0, keeper_loc[0], keeper_loc[1]),
                                (1, keeper_loc[0], keeper_loc[1]+1)]
            params['keeper_start'] = None
        else:
            raise ValueError("Incorrect direction!")

        # Set the boxes positions as starting position.
        if params['box'] is not None:
            params['box_start'] = params['box']
            params['box'] = None

        # Set the time to only two steps and box_end to None.
        params['time'] = (0, 1)
        params['box_end'] = None

        sol = self.run_game(params)[0]
        if sol['satisfiable'] is True:
            params['box'] = [(c, r) for t, c, r in sol['Box'] if t == 1]
            params['keeper_start'] = [params['keeper'][1][1:]]
            params['keeper'] = None
            params['box_end'] = self.params['box_end']
            self.params = params

        # Register the move.
        self.movecounter += 1
        self._boxes_pos[self.movecounter] = params['box']
        self._keeper_pos[self.movecounter] = params['keeper_start']
        return True

    def undo_move(self):
        """
        Method to go back to the previous move.
        """
        # Set the boxes and keeper positions to the previous move.
        self.movecounter -= 1
        try:
            self.params['box'] = self._boxes_pos[self.movecounter]
            self.params['box_start'] = []
            self.params['keeper_start'] = self._keeper_pos[self.movecounter]
            return True
        except KeyError:
            self.movecounter += 1
            return False

    def redo_move(self):
        """
        Method to redo a move.
        """
        # Set the boxes and keeper positions to the previous move.
        self.movecounter += 1
        try:
            self.params['box'] = self._boxes_pos[self.movecounter]
            self.params['box_start'] = []
            self.params['keeper_start'] = self._keeper_pos[self.movecounter]
            return True
        except KeyError:
            self.movecounter -= 1
            return False

    def check_solv(self):
        """
        Method which checks if a game can be solved in its current state.
        """
        # Copy the current set of params.
        params = self.params.copy()

        # Set the max time to 100. This might be higher for larger puzzles.
        params['time'] = (0, 150)

        # Set box start to the current box positions.
        if params['box']:
            params['box_start'] = params['box']
            params['box'] = None

        sol = self.run_game(params)[0]
        return sol['satisfiable']

    def find_sol(self):
        """
        Method to look for a solution.
        """
        # Copy the current set of params.
        params = self.params.copy()

        # Set the max time to 100. This might be higher for larger puzzles.
        params['time'] = (0, 150)

        # Set box start to the current box positions.
        if params['box']:
            params['box_start'] = params['box']
            params['box'] = None

        sol = self.run_game(params, box_end=True)[0]
        # Set the box positions to the right values.
        for rel_t, c, r in sol['Box']:
            abs_t = rel_t + self.movecounter
            try:
                self._boxes_pos[abs_t].append((c, r))
            except KeyError:
                self._boxes_pos[abs_t] = [(c, r)]

        for rel_t, c, r in sol['Keeper']:
            abs_t = rel_t + self.movecounter
            self._keeper_pos[abs_t] = [(c, r)]

    def find_opt_sol(self):
        """
        Method to look for a solution.
        """
        # Copy the current set of params.
        params = self.params.copy()

        # Set the max time to 100. This might be higher for larger puzzles.
        params['time'] = (0, 150)

        # Set box start to the current box positions.
        if params['box']:
            params['box_start'] = params['box']
            params['box'] = None

        sol = self.run_game(params, opt=True, box_end=True)[0]

        # Only remember the past positions.
        for i in range(self.movecounter, len(self._boxes_pos)):
            self._boxes_pos.pop(i)
            self._keeper_pos.pop(i)

        # Set the box positions to the right values.
        for rel_t, c, r in sol['Box']:
            abs_t = rel_t + self.movecounter
            try:
                self._boxes_pos[abs_t].append((c, r))
            except (KeyError, AttributeError):
                self._boxes_pos[abs_t] = [(c, r)]

        for rel_t, c, r in sol['Keeper']:
            abs_t = rel_t + self.movecounter
            self._keeper_pos[abs_t] = [(c, r)]

    def reset(self):
        self.movecounter = 0
        self.params = {'col': (0, 5),
                       'row': (0, 7),
                       'time': (0, 60),
                       'nb_moves': None,
                       'box_start': [(3, 1), (2, 2), (3, 3), (2, 4), (2, 5)],
                       'box_end': [(1, 1), (2, 1), (3, 1), (4, 1), (1, 2)],
                       'box': None,
                       'keeper_start': [(1, 5)],
                       'keeper': None,
                       'left': None,
                       'right': None,
                       'up': None,
                       'down': None,
                       'wall': [(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0),
                                (0, 1), (5, 1), (0, 2), (5, 2), (0, 3), (1, 3),
                                (5, 3), (0, 4), (1, 4), (4, 4), (5, 4), (0, 5),
                                (4, 5), (0, 6), (1, 6), (4, 6), (1, 7), (2, 7),
                                (3, 7), (4, 7)]}

    def gen_puzzle(self, dimensions):
        params = self.params.copy()

        params['col'] = (0, dimensions[0]-1)
        params['row'] = (0, dimensions[1]-1)
        params['box_start'] = None
        params['box_end'] = None
        params['box'] = None
        params['keeper_start'] = None
        params['keeper'] = None
        params['wall'] = None
        params['time'] = (0, 30)
        params['nb_moves'] = 1  # TODO REMOVE HARDCODE
        sol = self.run_game(params, nb_box=5, box_end=True, nb_models=1)[-1]
        if sol['satisfiable']:
            params['box_start'] = sol['Box_Start']
            params['box_end'] = sol['Box_End']
            params['keeper_start'] = sol['Keeper_Start']
            params['wall'] = sol['Wall']
            params['nb_moves'] = None
        self.params = params
        return True

    def run_game(self, params, opt=False, nb_box=False, box_end=False,
                 nb_models=1):
        if nb_box == 0:
            nb_box = len(params['box_start'])

        idp = IDP('idp')
        idp.nbmodels = nb_models
        # Define necessary types and predicates.
        idp.Type("col", params['col'])
        idp.Type("row", params['row'])
        idp.Type("time", params['time'])

        idp.Constant("Nb_Moves: time", params['nb_moves'])

        idp.Predicate("Box_Start(col, row)", params['box_start'])
        idp.Predicate("Box_End(col, row)", params['box_end'])
        idp.Predicate("Box(time, col, row)", params['box'])
        idp.Predicate("Keeper_Start(col, row)", params['keeper_start'])
        idp.Predicate("Keeper(time, col, row)", params['keeper'])
        idp.Predicate("Left(time)", params['left'])
        idp.Predicate("Right(time)", params['right'])
        idp.Predicate("Up(time)", params['up'])
        idp.Predicate("Down(time)", params['down'])
        idp.Predicate("Wall(col, row)", params['wall'])

        # Define movement of Keeper.
        idp.Define("!c[col], r[row]: Keeper(0, c, r) <- "
                   " Keeper_Start(c, r).\n"
                   " !t[time], c[col], r[row]: Keeper(t+1, c+1, r) <-"
                   " Keeper(t, c, r) & Right(t).\n"
                   " !t[time], c[col], r[row]: Keeper(t+1, c-1, r) <-"
                   " Keeper(t, c, r) & Left(t).\n"
                   " !t[time], c[col], r[row]: Keeper(t+1, c, r+1) <-"
                   " Keeper(t, c, r) & Down(t).\n"
                   " !t[time], c[col], r[row]: Keeper(t+1, c, r-1) <-"
                   " Keeper(t, c, r) & Up(t)\n", True)
        # There can only be at max one keeper.
        idp.Constraint("!t: ?=1c, r: Keeper(t, c, r).", True)
        # Define Box movement.
        idp.Define("!c, r: Box(0, c, r) <- Box_Start(c, r)."
                   " !c[col], r, t: Box(t+1, c, r) <-"
                   " Box(t, c, r) & ~(Keeper(t,c-1,r) & Right(t)) &"
                   " ~(Keeper(t,c+1,r) & Left(t)) & ~(Keeper(t,c,r+1) &"
                   " Up(t)) & ~(Keeper(t,c,r-1) & Down(t))."
                   " !c[col], r, t: Box(t+1, c+1, r) <- Box(t, c, r) &"
                   " Keeper(t,c-1,r) & Right(t)."
                   " !c[col], r, t: Box(t+1, c-1, r) <- Box(t, c, r) &"
                   " Keeper(t,c+1,r) & Left(t)."
                   " !c[col], r, t: Box(t+1, c, r-1) <- Box(t, c, r)"
                   " & Keeper(t,c,r+1) & Up(t)."
                   " !c[col], r, t: Box(t+1, c, r+1) <-"
                   " Box(t, c, r) & Keeper(t,c,r-1) &"
                   " Down(t).", True)
        # Every box needs to be in an end position.
        if box_end:
            if opt is True:
                idp.Constraint("!c, r: Box_End(c, r) <=> Box({}, c, r)."
                               .format("Nb_Moves"), True)
            else:
                idp.Constraint("!c, r: Box_End(c, r) <=> Box({}, c, r)."
                               .format(params['time'][1]), True)

        # Every turn, we need to move.
        idp.Constraint("!t[time]: (Right(t) & ~Left(t) & ~Down(t) &"
                       " ~Up(t)) | (Left(t) & ~Right(t) & ~Down(t) &"
                       " ~Right(t)) | (Down(t) & ~Right(t) & ~Up(t) &"
                       " ~Left(t)) | (Up(t) & ~Left(t) & ~Down(t) &"
                       " ~Right(t)).", True)

        # Constrain when movements are possible.
        idp.Constraint("!t, c, r: ~(Wall(c, r) & Box(t, c, r)).", True)
        idp.Constraint("!t, c, r: ~(Wall(c, r) & Keeper(t, c, r)).", True)
        idp.Constraint("!t, c, r: ~(Box(t, c, r) & Keeper(t, c, r)).", True)
        idp.Constraint("!t: #{{c, r: Box(t, c, r)}} = {}."
                       .format(nb_box),
                       True)

        if opt:
            return idp.minimize("Nb_Moves")
        else:
            return idp.model_expand()

    def toggle_tile(self, x, y, target_tile):
        """
        Changes a tile from one type into another.
        """
        field = self.generate_field()
        tile = field[y][x]
        self.remove_tile(x, y)
        if tile == target_tile:
            pass  # Toggle a tile if the same is already present.
        elif target_tile == 1:
            self.add_tile(x, y, 'wall')
        elif target_tile == 2:
            self.add_tile(x, y, 'box_start')
        elif target_tile == 3:
            self.add_tile(x, y, 'box_start')
            self.add_tile(x, y, 'box_end')
        elif target_tile == 4:
            self.add_tile(x, y, 'box_end')
        elif target_tile == 5:
            self.add_tile(x, y, 'keeper_start')

        return True

    def remove_tile(self, x, y):
        """
        Method to remove a tile.
        This involves removing it from every list in the params dictionary.
        """
        for key, vals in self.params.items():
            if vals and (x, y) in vals:
                vals.remove((x, y))
                self.params[key] = vals

    def add_tile(self, x, y, tile_type):
        """
        Method to add a tile of any type.
        """
        if self.params[tile_type] is None or self.params[tile_type] == ['']:
            self.params[tile_type] = [(x, y)]
        else:
            self.params[tile_type].append((x, y))


class Sudoku(Game):
    """

    """
    def __init__(self):
        self.movecounter = 0

        self.params = {'col': (1, 9),
                       'row': (1, 9),
                       'val': (1, 9),
                       'inputsquare': [(1, 1, 8), (1, 2, 5), (1, 6, 2),
                                       (1, 7, 4), (2, 1, 7), (2, 2, 2),
                                       (2, 9, 9), (3, 3, 4), (4, 4, 1),
                                       (4, 6, 7), (4, 9, 2), (5, 1, 3),
                                       (5, 3, 5), (5, 7, 9), (6, 2, 4),
                                       (7, 5, 8), (7, 8, 7), (8, 2, 1),
                                       (8, 3, 7), (9, 5, 3), (9, 6, 6),
                                       (9, 8, 4)],
                       'cellval': None,
                       'square': None}
        self.generate_field()

    def generate_field(self):
        """
        Functions which generates the game field.
        Every number has its own identifier.

        * 0 = empty
        * 1 = one
        * 2 = two
        ...

        """
        field = []

        # If no cellval have been set yet, use the input to create a field.
        # Cellval are only set once the system has been ran.
        if not self.params['cellval']:
            for r in range(1, 10):
                row = []
                for c in range(1, 10):
                    valfound = False
                    for val in range(1, 10):
                        if (r, c, val) in self.params['inputsquare']:
                            valfound = True
                            row.append(val)
                    if not valfound:
                        row.append(0)
                field.append(row)

        # If there already exist cellval, use those.
        # TODO FIX THIS CODE
        else:
            for r in range(self.params['row'][0], self.params['row'][1]+1):
                row = []
                for c in range(self.params['col'][0], self.params['col'][1]+1):
                    if self.params['cellval'] and (c, r) in self.params['cellval']:
                        row.append(self.params['cellval'][(c, r)])
                    else:
                        row.append(0)
                field.append(row)
        return field

    def set_field(self, field):
        """
        Method to set the functions and predicates based on the field.
        This basically does the opposite from `get_field`.

        * 0 = zero
        * 1 = one
        * ...
        * 9 = nine
        """
        # Clear out the old variables.
        self.params = {'col': (1, 9),
                       'row': (1, 9),
                       'val': (1, 9),
                       'inputsquare': [(1, 1, 8), (1, 2, 5), (1, 6, 2),
                                       (1, 7, 4), (2, 1, 7), (2, 2, 2),
                                       (2, 9, 9), (3, 3, 4), (4, 4, 1),
                                       (4, 6, 7), (4, 9, 2), (5, 1, 3),
                                       (5, 3, 5), (5, 7, 9), (6, 2, 4),
                                       (7, 5, 8), (7, 8, 7), (8, 2, 1),
                                       (8, 3, 7), (9, 5, 3), (9, 6, 6),
                                       (9, 8, 4)],
                       'cellval': None,
                       'square': None}
        # Set the new variables.
        for r, line in enumerate(field):
            for c, el in enumerate(line):
                if el == 0:
                    continue
                elif el == 1:
                    self.add_tile(c, r, 1)
                elif el == 2:
                    self.add_tile(c, r, 2)
                elif el == 3:
                    self.add_tile(c, r, 3)
                elif el == 4:
                    self.add_tile(c, r, 3)
                elif el == 5:
                    self.add_tile(c, r, 3)
                elif el == 6:
                    self.add_tile(c, r, 3)
                elif el == 7:
                    self.add_tile(c, r, 3)
                elif el == 8:
                    self.add_tile(c, r, 3)
                elif el == 9:
                    self.add_tile(c, r, 3)

    def do_move(self, direction):
        """
        This function is unnecessary for Sudoku.
        """
        return True

    def undo_move(self):
        """
        This function is unnecessary for Sudoku.
        """
        return True

    def redo_move(self):
        """
        This function is unnecessary for Sudoku.
        """
        return True

    def check_solv(self):
        """
        Method which checks if the Sudoku can be solved in its current state.
        """
        # Copy the current set of params.
        params = self.params.copy()

        sol = self.run_game(params)[0]
        return sol['satisfiable']

    def find_sol(self):
        """
        Method to look for a solution.
        """
        # Copy the current set of params.
        params = self.params.copy()

        # Run the game.
        sol = self.run_game(params)[0]

        # Fill the params dictionary.
        inputsquare = []
        for (col, row), val in sol['CellVal'].items():
            inputsquare.append((col, row, val))

        self.params['inputsquare'] = inputsquare


        """
        Method to look for a solution.
        """
        # Copy the current set of params.
        params = self.params.copy()

        # Set the max time to 100. This might be higher for larger puzzles.
        params['time'] = (0, 150)

        # Set box start to the current box positions.
        if params['box']:
            params['box_start'] = params['box']
            params['box'] = None

        sol = self.run_game(params, opt=True, box_end=True)[0]

        # Only remember the past positions.
        for i in range(self.movecounter, len(self._boxes_pos)):
            self._boxes_pos.pop(i)
            self._keeper_pos.pop(i)

        # Set the box positions to the right values.
        for rel_t, c, r in sol['Box']:
            abs_t = rel_t + self.movecounter
            try:
                self._boxes_pos[abs_t].append((c, r))
            except (KeyError, AttributeError):
                self._boxes_pos[abs_t] = [(c, r)]

        for rel_t, c, r in sol['Keeper']:
            abs_t = rel_t + self.movecounter
            self._keeper_pos[abs_t] = [(c, r)]

    def reset(self):
        self.params = {'col': (1, 9),
                       'row': (1, 9),
                       'val': (1, 9),
                       'inputsquare': [(1, 1, 8), (1, 2, 5), (1, 6, 2),
                                       (1, 7, 4), (2, 1, 7), (2, 2, 2),
                                       (2, 9, 9), (3, 3, 4), (4, 4, 1),
                                       (4, 6, 7), (4, 9, 2), (5, 1, 3),
                                       (5, 3, 5), (5, 7, 9), (6, 2, 4),
                                       (7, 5, 8), (7, 8, 7), (8, 2, 1),
                                       (8, 3, 7), (9, 5, 3), (9, 6, 6),
                                       (9, 8, 4)],
                       'cellval': None,
                       'square': None}

    def gen_puzzle(self, dimensions):
        params = self.params.copy()

        params['col'] = (0, dimensions[0]-1)
        params['row'] = (0, dimensions[1]-1)
        params['box_start'] = None
        params['box_end'] = None
        params['box'] = None
        params['keeper_start'] = None
        params['keeper'] = None
        params['wall'] = None
        params['time'] = (0, 30)
        params['nb_moves'] = 1  # TODO REMOVE HARDCODE
        sol = self.run_game(params, nb_box=5, box_end=True, nb_models=1)[-1]
        if sol['satisfiable']:
            params['box_start'] = sol['Box_Start']
            params['box_end'] = sol['Box_End']
            params['keeper_start'] = sol['Keeper_Start']
            params['wall'] = sol['Wall']
            params['nb_moves'] = None
        self.params = params
        return True

    def run_game(self, params, opt=False, nb_models=1):
        """
        "Run" the game.
        We input the set of params to the IDP system, after which it will
        expand these to a full solution.

        Different actions require different input information, but always the
        same logic.

        * <action>: <inputs> -> <outputs>
        * check solv: col, row, val, inputsquare -> cellval
        * find sol: col, row, val, inputsquare -> cellval
        * gen puzzle: col, row -> inputsquare, cellval
        """
        idp = IDP('idp')
        idp.nbmodels = nb_models
        # Define necessary types and predicates.
        idp.Type("col", params['col'])
        idp.Type("row", params['row'])
        idp.Type("val", params['val'])

        idp.Predicate("InputSquare(col, row, val)", params['inputsquare'])
        idp.Function("CellVal(col, row): val", params['cellval'])
        idp.Predicate("Square(col, row, col, row)", params['square'])

        # Every value in the inputsquare also needs to be a cellval.
        idp.Constraint("!col, row, v: InputSquare(col, row, v) => "
                       "CellVal(col, row) = v.", True)

        # Every value can only appear once on every col and row.
        idp.Constraint("!col val: ?=1 row: CellVal(col, row) = val.", True)
        idp.Constraint("!row val: ?=1 col: CellVal(col, row) = val.", True)

        # Calculate what cells are in the same squares.
        idp.Constraint("!col1, col2, row1, row2: (col1 - (col1-1)%3) ="
                       " (col2 - (col2-1)%3) & (row1 - (row1-1)%3) ="
                       " (row2 - (row2-1)%3) & ~(col1 = col2 & row1 = row2)"
                       " <=> Square(col1, row1, col2, row2).", True)

        # Two cells in the same square cannot have the same value.
        idp.Constraint("!col1, col2, row1, row2:"
                       " Square(col1, row1, col2, row2) =>"
                       " CellVal(col1, row1) ~= CellVal(col2, row2).",
                       True)
        if opt:
            raise ValueError("Nothing to optimize.")
        else:
            return idp.model_expand()

    def toggle_tile(self, x, y, target_tile):
        """
        Changes a tile from one type into another.
        Except for 9, every tile is switched into the tile + 1.
        When toggling a tile containing a 9, we simply remove it.
        """
        field = self.generate_field()
        tile = field[y][x]
        self.remove_tile(x + 1, y + 1, tile)
        if tile != 9:
            self.add_tile(x + 1, y + 1, tile + 1)
        return True

    def remove_tile(self, x, y, tile):
        """
        Method to remove a tile.
        """
        if self.params['inputsquare'] and (y, x, tile) in self.params['inputsquare']:
            self.params['inputsquare'].remove((y, x, tile))

    def add_tile(self, x, y, tile_type):
        """
        Method to add a tile of any type.
        """
        if self.params['inputsquare'] is None or self.params['inputsquare'] == ['']:
            self.params['inputsquare'] = [(y, x, tile_type)]
        else:
            self.params['inputsquare'].append((y, x, tile_type))


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    game = sys.argv[1]

    widget = Main_Widget(game)
    # widget = Play_Grid_Widget()
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())

